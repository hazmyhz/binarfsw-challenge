const express = require('express');
const router = express.Router();
const pageController = require('../controllers/page');

router.get('/', pageController.loginPage);
router.get('/home', pageController.homePage);
router.get('/game', pageController.gamePage);
router.get('/users', pageController.userInfo);

module.exports = router;
