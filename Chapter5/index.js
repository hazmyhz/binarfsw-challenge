const express = require('express');
const { loadUsers } = require('./utils/user.js')
const expressLayouts = require('express-ejs-layouts');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const bodyParser = require('body-parser')

const pageRoutes = require('./routes/page');
const userRoutes = require('./routes/user');

const app = express();
const port = 8000;

//EJS
app.set('view engine', 'ejs');
//Third-party Middlewere
app.use(expressLayouts);
//Built-in middleware
app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }));

//Konfigurasi flash
app.use(cookieParser('secret'));
app.use(session({
    cookie: { maxAge: 6000 },
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
}));
app.use(flash());
app.use(bodyParser.json())

app.use(('/'), pageRoutes);
app.use(('/'), userRoutes);

app.use('/', (req, res) => {
    res.status(404);
    res.send('<h1>404</h1>');
})


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})