const { loginCheck, duplicateCheck, addUser } = require('../utils/user')

class userController {
    static registerUser(req, res) {
        const isDuplicated = duplicateCheck(req.body.username)
        if (isDuplicated) {
            res.render('login', {
                title: "Login Traditional Game",
                layout: 'layouts/login-layout',
                errors: "Username has been taken, please use other username!"
            })
        } else {
            addUser(req.body);
            res.render('login', {
                title: "Login Traditional Game",
                layout: 'layouts/login-layout',
                success: "Register Success"
            })
        }
    }
    static loginUser(req, res) {
        const login = loginCheck(req.body.username, req.body.password)
        if (login === true) {
            res.redirect('/home');
        } else {
            res.render('login', {
                title: 'Login Traditional Games',
                layout: 'layouts/login-layout',
                errors: login
            });
        }
    }
}
module.exports = userController;