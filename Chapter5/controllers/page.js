const { loadUsers } = require('../utils/user')

class userController {
    static loginPage(req, res) {
        res.render('login', {
            title: 'Login Traditional Games',
            layout: 'layouts/login-layout',
        });
    }
    static homePage(req, res) {
        res.render('home', {
            title: 'Traditional Games',
            layout: 'layouts/main-layout',
        });
    }
    static gamePage(req, res) {
        res.render('game', {
            title: 'Rock-Paper-Scissors',
            layout: 'layouts/game-layout',
        });
    }
    static userInfo(req, res) {
        const users = loadUsers();
        res.jsonp(users);
    }
}
module.exports = userController;