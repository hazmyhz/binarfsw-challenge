let rock = 'rock', paper = 'paper', scissors = 'scissors'

const selections = document.querySelectorAll('#player');
const refresh = document.querySelector('.refresh-game');
const gameResult = document.querySelector('.game-result');
const scoreInfo = document.querySelector('.score');
const comSelections = document.querySelectorAll('#com');
gameResult.style.visibility = "hidden"


const getCom = () => {
    let ranNumber = Math.ceil(Math.random() * 10);
    return selection = (ranNumber <= 4) ? rock : (ranNumber > 4 && ranNumber <= 7) ? paper : scissors;
}

const getResult = (player1, comPlayer) => {
    return result = (player1 === comPlayer) ? 'Draw'
        : (player1 === rock && comPlayer === scissors)
            || (player1 === paper && comPlayer === rock)
            || (player1 === scissors && comPlayer === paper) ? `You Win !!!` : `You Lose !!!`
}

const refreshGame = () => {
    gameResult.style.visibility = "hidden";
    selections.forEach(function (select) {
        select.classList.remove('active');
        select.classList.remove('shake');
    })
    comSelections.forEach(function (comSelect) {
        comSelect.classList.remove('active');
        comSelect.classList.remove('shake');
    })
}

refresh.addEventListener('click', function () {
    refreshGame()
    score = parseInt(0)
    scoreInfo.innerHTML = score
})


const randomCom = () => {
    const startTime = new Date().getTime()
    setInterval(function () {
        if (new Date().getTime() - startTime > 3000) {
            clearInterval;
            return;
        }
        comSelections.forEach(function (comSelect, i) {
            setTimeout(function () {
                comSelect.classList.add("active");
            }, (i + 1) * 100)
            setTimeout(function () {
                comSelect.classList.remove("active");
            }, (i + 1) * 200)
            setTimeout(function () {
                if (comSelect.classList.contains(comSelection)) {
                    comSelect.classList.add("active")
                    comSelect.classList.add("shake")
                }
            }, 2000)
        })
    }, 1000)
}

let score = parseInt(0)
let comSelection;

selections.forEach(function (select) {
    select.addEventListener('click', function () {
        refreshGame()
        const playerSelection = select.className
        comSelection = getCom();
        const result = getResult(playerSelection, comSelection)
        randomCom()
        select.classList.add("active")
        gameResult.innerHTML = result
        result === 'Draw' ? gameResult.style.background = "grey"
            : result === 'You Win !!!' ? (gameResult.style.background = "green", score = parseInt(score + 1))
                : (gameResult.style.background = "red", score = parseInt(score - 1))

        setTimeout(function () {
            gameResult.style.visibility = "visible"
            select.classList.add("shake")
            scoreInfo.innerHTML = score;
        }, 3000)

    }
    )
})