let modal = document.getElementById("myModal");
let modalInfo = document.getElementById("user-info-modal");

// Get the button that opens the modal
let btn = document.getElementById("myBtn");
let btnInfo = document.getElementById("user-info-btn");

// Get the <span> element that closes the modal
let span = document.getElementsByClassName("close")[0];
let spanInfo = document.getElementsByClassName("closeInfo")[0];

// When the user clicks the button, open the modal 
btn.onclick = function () {
    modal.style.display = "flex";
}

btnInfo.onclick = function () {
    modalInfo.style.display = "flex";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

spanInfo.onclick = function () {
    modalInfo.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    if (event.target == modalInfo) {
        modalInfo.style.display = "none";
    }
}

const userInfo = document.getElementById("json-info");

async function getUsers() {
    let data = await fetch('http://localhost:8000/users', { method: "get" })
        .then(response => {
            return response.json()
        })
        .then(response => {
            userInfo.innerHTML = JSON.stringify(response, undefined, 2)
            return response;
        });
    return data;
}

const userJSON = getUsers()