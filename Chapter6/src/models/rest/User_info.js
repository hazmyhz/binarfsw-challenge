
const user_info = (sequelize, DataTypes) => {
    const User_info = sequelize.define(
        'user_info',
        {
            user_id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                // references: {
                //     model: 'user',
                //     key: 'id'
                // }
            },
            age: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            gender: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            city: {
                type: DataTypes.STRING,
                allowNull: false,
            },
        },
        {
            timestamps: true,
            freezeTableName: true,
        }
    );

    User_info.sync();
    return User_info;
};

module.exports = user_info