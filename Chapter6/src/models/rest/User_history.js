
const user_history = (sequelize, DataTypes) => {
    const User_history = sequelize.define(
        'user_history',
        {
            user_id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
            },
            time_play: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            total_match: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            win: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            lose: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            win_rate: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
        },
        {
            timestamps: true,
            freezeTableName: true,
        }
    );

    User_history.sync();
    return User_history;
};

module.exports = user_history