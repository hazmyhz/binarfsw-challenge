const { loadUsers } = require('../utils/user')

class pageController {
    static loginPage(req, res) {
        res.render('login', {
            title: 'Login Traditional Games',
            layout: 'layouts/login-layout',
        });
    }
    static homePage(req, res) {
        res.render('home', {
            title: 'Traditional Games',
            layout: 'layouts/main-layout',
        });
    }
    static gamePage(req, res) {
        res.render('game', {
            title: 'Rock-Paper-Scissors',
            layout: 'layouts/game-layout',
        });
    }
    static profilePage(req, res) {
        res.render('profile', {
            title: 'Profile',
            layout: 'layouts/main-layout',
        });
    }
    static adminPage(req, res) {
        res.render('admin', {
            title: 'Admin',
            layout: 'layouts/main-layout',
        });
    }
    static userAccountPage(req, res) {
        res.render('useraccount', {
            title: 'User Account',
            layout: 'layouts/main-layout',
        });
    }
    static userInformationPage(req, res) {
        res.render('userinformation', {
            title: 'User Information',
            layout: 'layouts/main-layout',
        });
    }
    static userHistoryPage(req, res) {
        res.render('userhistory', {
            title: 'User History',
            layout: 'layouts/main-layout',
        });
    }
    static userInfo(req, res) {
        const users = loadUsers();
        res.jsonp(users);
    }
}
module.exports = pageController;