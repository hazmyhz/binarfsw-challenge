const db = require('../models');
const History = db.rest.models.user_history;

exports.getUsersHistory = async (req, res) => {

    const histories = await History.findAll();

    if (histories.length === 0) {
        return res.status(200).send({
            message: `user information not available`,
        });
    }

    return res.send(histories);
};

exports.createHistory = async (req, res) => {
    const { user_id, win, lose } = req.body;
    const total_match = parseInt(win + lose)
    const time_play = parseInt(total_match * 10)
    const win_rate = parseInt(total_match / win)

    try {
        let newHistory = await History.create({
            user_id,
            time_play,
            total_match,
            win,
            lose,
            win_rate
        });
        return res.redirect('/admin')
    } catch (err) {
        return res.status(500).send({
            message: `Error: ${err.message}`,
        });
    }
};

exports.deleteHistory = async (req, res) => {
    const { user_id } = req.params
    if (!user_id) {
        return res.status(400).send({
            message: 'Please provide a id for the user you are trying to delete!',
        });
    }

    const history = await History.findOne({
        where: {
            user_id,
        },
    });

    if (!history) {
        return res.status(400).send({
            message: `No user found with the id ${user_id}`,
        });
    }

    try {
        await history.destroy();
        return res.redirect('/admin')
    } catch (err) {
        return res.status(500).send({
            message: `Error: ${err.message}`,
        });
    }
};

// exports.updateUser = async (req, res) => {
//     const { username, password } = req.body;
//     const { id } = req.params;

//     const user = await User.findOne({
//         where: {
//             id,
//         },
//     });

//     if (!user) {
//         return res.status(400).send({
//             message: `No user found with the id ${id}`,
//         });
//     }

//     try {
//         if (username) {
//             user.username = username;
//         }
//         if (password) {
//             user.password = password;
//         }

//         user.save();
//         return res.send({
//             message: `User ${id} has been updated!`,
//         });
//     } catch (err) {
//         return res.status(500).send({
//             message: `Error: ${err.message}`,
//         });
//     }
// };
