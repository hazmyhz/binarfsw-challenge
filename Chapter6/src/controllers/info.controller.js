const db = require('../models');
const Info = db.rest.models.user_info;

exports.getUsersInfo = async (req, res) => {

    const users = await Info.findAll({
        attributes: ['user_id', 'age', 'gender', 'city']
    });

    if (users.length === 0) {
        return res.status(200).send({
            message: `user information not available`,
        });
    }

    return res.send(users);
};

exports.createInfo = async (req, res) => {
    const { user_id, age, gender, city } = req.body;

    try {
        let newInfo = await Info.create({
            user_id,
            age,
            gender,
            city
        });
        return res.redirect('/admin')
    } catch (err) {
        return res.status(500).send({
            message: `Error: ${err.message}`,
        });
    }
};

exports.deleteInfo = async (req, res) => {
    const { user_id } = req.params
    if (!user_id) {
        return res.status(400).send({
            message: 'Please provide a id for the user you are trying to delete!',
        });
    }

    const info = await Info.findOne({
        where: {
            user_id,
        },
    });

    if (!info) {
        return res.status(400).send({
            message: `No user found with the id ${user_id}`,
        });
    }

    try {
        await info.destroy();
        return res.redirect('/admin')
    } catch (err) {
        return res.status(500).send({
            message: `Error: ${err.message}`,
        });
    }
};

// exports.updateUser = async (req, res) => {
//     const { username, password } = req.body;
//     const { id } = req.params;

//     const user = await User.findOne({
//         where: {
//             id,
//         },
//     });

//     if (!user) {
//         return res.status(400).send({
//             message: `No user found with the id ${id}`,
//         });
//     }

//     try {
//         if (username) {
//             user.username = username;
//         }
//         if (password) {
//             user.password = password;
//         }

//         user.save();
//         return res.send({
//             message: `User ${id} has been updated!`,
//         });
//     } catch (err) {
//         return res.status(500).send({
//             message: `Error: ${err.message}`,
//         });
//     }
// };
