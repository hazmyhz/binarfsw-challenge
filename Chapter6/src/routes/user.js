const express = require('express');
const router = express.Router();
const user = require('../controllers/user.controller');

router.get('/:id', user.getUser);

router.get('/', user.getUsers);

router.post('/loginUser', user.loginUser);

router.post('/registerUser', user.registerUser);

router.post('/createUser', user.createUser);

router.get('/delete/:id', user.deleteUser);

router.post('/update/:id', user.updateUser);

module.exports = router;
