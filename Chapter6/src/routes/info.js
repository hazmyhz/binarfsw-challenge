const express = require('express');
const router = express.Router();
const info = require('../controllers/info.controller');

router.get('/', info.getUsersInfo);

router.post('/createInfo', info.createInfo);

router.get('/deleteInfo/:user_id', info.deleteInfo);

// router.post('/updateInfo/:id', info.updateInfo);

module.exports = router;
