const express = require('express');
const router = express.Router();
const history = require('../controllers/history.controller');

router.get('/', history.getUsersHistory);

router.post('/createHistory', history.createHistory);

router.get('/deleteHistory/:user_id', history.deleteHistory);

// router.post('/updateInfo/:id', info.updateInfo);

module.exports = router;
