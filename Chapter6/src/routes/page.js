const express = require('express');
const router = express.Router();
const pageController = require('../controllers/page');

router.get('/', pageController.loginPage);
router.get('/login', pageController.loginPage);
router.get('/home', pageController.homePage);
router.get('/game', pageController.gamePage);
router.get('/users', pageController.userInfo);
router.get('/profile', pageController.profilePage);
router.get('/admin', pageController.adminPage);
router.get('/userAccount', pageController.userAccountPage);
router.get('/userInformation', pageController.userInformationPage);
router.get('/userHistory', pageController.userHistoryPage);

module.exports = router;
