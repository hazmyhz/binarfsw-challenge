import user from './user';
import page from './page';
import info from './info';
import history from './history';

export default {
  user,
  page,
  info,
  history,
};
