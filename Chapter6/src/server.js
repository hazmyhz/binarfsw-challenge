import 'dotenv/config';
import express from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import fs from 'fs';
import path from 'path';
import routes from './routes';
import expressLayouts from 'express-ejs-layouts';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import flash from 'connect-flash';
import bodyParser from 'body-parser';
import pageRoutes from './routes/page';

const app = express();

const accessLogStream = fs.createWriteStream(
  path.join(__dirname, '../access.log'),
  { flags: 'a' }
);

app.set('views', './views');
app.set('view engine', 'ejs');
app.use(expressLayouts)
app.use(express.static('public'))
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: true, limit: '50mb' }));

app.use(helmet());
app.use(morgan('combined', { stream: accessLogStream }));

app.use(cookieParser('secret'));
app.use(session({
  cookie: { maxAge: 6000 },
  secret: 'secret',
  resave: true,
  saveUninitialized: true,
}));

app.use(flash());
app.use(bodyParser.json());

app.use(('/'), pageRoutes);
app.use('/user', routes.user);
app.use('/info', routes.info);
app.use('/history', routes.history);

app.use('/', (req, res) => {
  res.status(404).send('404: Page not found');
});

app.listen(process.env.PORT, () => {
  console.log(`Example app listening on port http://localhost:${process.env.PORT}`);
});
