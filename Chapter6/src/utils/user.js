const fs = require('fs');

//Memeriksa Folder
const dirPath = './data';
if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath);
}

//Memeriksa File contacts.json
const dataPath = './data/user.json';
if (!fs.existsSync(dataPath)) {
    fs.writeFileSync(dataPath, '[]', 'utf-8');
}

const loadUsers = () => {
    const file = fs.readFileSync('data/user.json', 'utf-8');
    const users = JSON.parse(file);
    return users;
}

const findUser = (username) => {
    const users = loadUsers();
    const user = users.find((user) => user.username.toLowerCase() === username.toLowerCase());
    return user;
};

const loginCheck = (username, password) => {
    const users = loadUsers();
    const user = users.find((user) => user.username.toLowerCase() === username.toLowerCase());
    const pass = users.find((user) => user.password === password);

    if (user && pass) {
        return true
    }

    if (!user) {
        return 'Your account is not available, please register!'
    }
    if (!pass) {
        return 'Please input correct password!'
    }
}

loginCheck('hazmy', 'binarians')

const saveUser = (users) => {
    fs.writeFileSync('data/user.json', JSON.stringify(users));
}


const addUser = (body) => {
    const users = loadUsers();
    users.push(body);
    saveUser(users);
};

const duplicateCheck = (username) => {
    const users = loadUsers();
    return users.find((user) => user.username.toLowerCase() === username.toLowerCase());
}

const deleteUser = (username) => {
    const users = loadUsers();
    const filteredUsers = users.filter((user) => user.username !== username);
    saveUser(filteredUsers);
}


module.exports = { loadUsers, findUser, addUser, duplicateCheck, deleteUser, loginCheck };
// ######################################################################