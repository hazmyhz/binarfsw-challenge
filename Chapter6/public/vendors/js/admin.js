async function getUsers(element) {
    let data = await fetch('http://localhost:9000/user', { method: "get" })
        .then(response => {
            return response.json()
        })
        .then(response => {
            let table = '<table>';
            table += `<thead>
                        <tr>
                            <th>id</th>
                            <th>username</th>
                            <th>password</th>
                            <th>actions</th>
                        </tr>
                        </thead>
                    <tbody>`;

            response.forEach(function (user) {
                table += '<tr><td>' + user.id + '</td>';
                table += '<td>' + user.username + '</td>';
                table += '<td>' + user.password + '</td>';
                table += `<td><a href="http://localhost:9000/user/delete/${user.id}" >delete</a>  <a href="http://localhost:9000/user/update/${user.id}">edit</a></td>`
            })

            table += '</tbody>';
            table += '</table>';

            element.innerHTML = table;
        });
    return data;
}

async function getUsersInfo(element) {
    let data = await fetch('http://localhost:9000/info', { method: "get" })
        .then(response => {
            return response.json()
        })
        .then(response => {
            let table = '<table>';
            table += `<thead>
                        <tr>
                            <th>id</th>
                            <th>gender</th>
                            <th>age (years)</th>
                            <th>city</th>
                            <th>actions</th>
                        </tr>
                        </thead>
                    <tbody>`;

            response.forEach(function (info) {
                table += '<tr><td>' + info.user_id + '</td>';
                table += '<td>' + info.age + '</td>';
                table += '<td>' + info.gender + '</td>';
                table += '<td>' + info.city + '</td>';
                table += `<td><a href="http://localhost:9000/info/deleteInfo/${info.user_id}" >delete</a>  <a href="http://localhost:9000/info/updateInfo/${info.user_id}" >edit</a></td>`
            })

            table += '</tbody>';
            table += '</table>';

            element.innerHTML = table;
        });
    return data;
}

async function getUsersHistoris(element) {
    let data = await fetch('http://localhost:9000/history', { method: "get" })
        .then(response => {
            return response.json()
        })
        .then(response => {
            let table = '<table>';
            table += `<thead>
                        <tr>
                            <th>id</th>
                            <th>time play (min)</th>
                            <th>total match</th>
                            <th>win</th>
                            <th>lose</th>
                            <th>win rate (%)</th>
                            <th>actions</th>
                        </tr>
                        </thead>
                    <tbody>`;

            response.forEach(function (history) {
                table += '<tr><td>' + history.user_id + '</td>';
                table += '<td>' + history.time_play + '</td>';
                table += '<td>' + history.total_match + '</td>';
                table += '<td>' + history.win + '</td>';
                table += '<td>' + history.lose + '</td>';
                table += '<td>' + history.win_rate + '</td>';
                table += `<td><a href="http://localhost:9000/history/deleteHistory/${history.user_id}" >delete</a>  <a href="http://localhost:9000/history/updateHistory/${history.user_id}" >edit</a></td>`
            })

            table += '</tbody>';
            table += '</table>';

            element.innerHTML = table;
        });
    return data;
}

async function getUsers_id(element) {
    let data = await fetch('http://localhost:9000/user', { method: "get" })
        .then(response => {
            return response.json()
        })
        .then(response => {
            let select_id = `<option value="" disabled selected>Choose user</option>`

            response.forEach(function (user) {
                select_id += `<option value=${user.id}>${user.username}</option>`;
            })

            element.innerHTML = select_id;
        });
    return data;
}




document.addEventListener('DOMContentLoaded', function () {
    const userAccount = document.getElementById("userAccount");
    const userInfo = document.getElementById("userInfo");
    const userHistoris = document.getElementById("userHistoris");
    const user_id = document.getElementById("user_id")

    getUsers_id(user_id)
    getUsers(userAccount)
    getUsersInfo(userInfo)
    getUsersHistoris(userHistoris)
}, false);